﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesHomework
{
    public class Pet
    {
        public string Name { get; set; }
        public int numOfLegs { get; set; }
        public Pet(string name, int numOfLegs)
        {
            Name = name;
            this.numOfLegs = numOfLegs;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace DelegatesHomework
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Pet> list = new List<Pet>() { 
                new Pet("Cat", 4),
                new Pet("Bird", 2),
                new Pet("Bug", 8),
                new Pet("Snake", 0),
                new Pet("Centipede", 40)};
            var maxLegsAnimal = list.GetMax<Pet>((e) => { return (float)e.numOfLegs; });
            Console.WriteLine($"Animal with maximum legs is {maxLegsAnimal.Name}");
            Console.WriteLine($"It has {maxLegsAnimal.numOfLegs} legs");

            var searcher = new FileSearcher();
            searcher.Notify += Searcher_FileFound;
            searcher.Start("C:\\source");
        }

        private static void Searcher_FileFound(object? sender, FileArgs e)
        {
            Console.WriteLine(e.FileName);
            if(e.FileName == "test")
            {
                var obj = (FileSearcher)sender;
                obj.Notify -= Searcher_FileFound;
            }
        }
    }
}
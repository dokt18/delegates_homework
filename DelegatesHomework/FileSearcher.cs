﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesHomework
{
    public class FileSearcher
    {
        public delegate void FileFound(object? sender, FileArgs e);
        public event FileFound? Notify;

        public void Start(string directory)
        {
            string[] files = Directory.GetFiles(directory, "*", SearchOption.AllDirectories);
            foreach (string file in files)
            {
                Notify?.Invoke(this, new FileArgs(Path.GetFileName(file)));
            }

        }
    }


    public class FileArgs : EventArgs
    {
        public string FileName { get; set; }
        public FileArgs(string fileName)
        {
            FileName = fileName;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesHomework
{
    public static class MaxSearch
    {
        public static T GetMax<T>(this IEnumerable e, Func<T, float> getParameter) where T : class
        {
            T max = null;
            var maxValue = float.MinValue;
            foreach(var item in e)
            {
                if(max == null)
                {
                    max = (T)item;
                    maxValue = getParameter((T)item);
                }
                var val = getParameter((T)item);
                if(val > maxValue)
                {
                    max = (T)item;
                    maxValue = getParameter((T)item);
                }
            }
            return max;
        }
    }
}
